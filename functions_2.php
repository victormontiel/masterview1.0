<?php
function createInnerMenu($Rol, $dbLoad){
    if(empty($Rol)) {
		
		return;
	}
	$retrievedViews = retrieveViewListTables($dbLoad);
	$viewArray=array("views" => $retrievedViews);

	$jsonResponse = json_encode($viewArray);

    return $jsonResponse;
}

function retrieveViewListTables($mainDB) {
	// include ("Mysqlconn.php");

	// Query to gather names of DBs of other tabs, for example proces
	// $query = "...";

    // $dbNames_sql = sqlsrv_query( $conexion, $query);
    // $dbNames = array();

	// while ($row = sqlsrv_fetch_array($dbNames_sql, SQLSRV_FETCH_NUMERIC)) 
	// {
	// 	if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
	// 	array_push($dbNames, $row[0]);		
    // }

	// Once all dbs of a menu are listed, the Views are retrieved.

	// foreach ($dbNames as $faKey => $dbName) {
	// 	$query = "SELECT View_Alias from ...";
	// 	$Views_sql = sqlsrv_query( $conexion, $query);
	// 	$Views = array();
	
	// 	while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
	// 	{
	// 		if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
	// 		array_push($Views, $row[0]);		
	// 	}
	// }

	// HardCoded Views

	$Views = array("Asistencia Firma Contrato", "Firma mandato SEPA", "Alta llamada", "Modificacion cuentas");
    return $Views;
	}

	function createItemTable($Rol, $item){
			// HARDCODED VERSION
			$dbName = "";
			$tableName ="";
			switch ($item) {
				case 'AsistenciaFirmaContrato':
					$dbName = "GAC_Asistencia_Firma_Contrato";
					$tableName = 'GAC_Asistencia_Firma_Contrato_DV_Master_View';
					break;
				case 'FirmamandatoSEPA':
					$dbName = "GOC_Getion_out_Firma_mandato_SEPA";
					$tableName = 'GOC_Gestion_out_Firma_mandato_SEPA_DV_Master_View';
					break;
				case 'Altallamada':
					$dbName = 'GOC_Llamada_in_Alta_llamada';
					$tableName = 'GOC_Alta_llamada_DV_Master_View';
					break;
				case 'ModificacionCuentas':
					$dbName = 'GOC_Modificacion_Cuentas';
					$tableName = 'GOC_Modificacion_Cuentas_DV_Master_View';
					break;
				default:
					return "view not contemplated YET";
					break;
			}
			if(empty($Rol)) {
				//displayLogInErrorMessage();
				redirectToLogIn();
				return;
			}
			//cambiar a tables
			$table = array();
			array_push($table, ["name"=>$item, "columns"=>getColumnNamesAndTypesItem($tableName, $dbName)]);
			$tableResponse = $table;
			$tableJson = json_encode($tableResponse);
			return $tableJson;
	}

	function fetchItemData($AliasName, $Rol){
		include ("Mysqlconn.php");
		// HARDCODE
		switch ($AliasName) {
			case 'AsistenciaFirmaContrato':
				$dbName = "GAC_Asistencia_Firma_Contrato";
				$View = 'GAC_Asistencia_Firma_Contrato_DV_Master_View';
				break;
			case 'FirmamandatoSEPA':
				$dbName = "GOC_Getion_out_Firma_mandato_SEPA";
				$View = 'GOC_Gestion_out_Firma_mandato_SEPA_DV_Master_View';
				break;
			case 'Altallamada':
				$dbName = 'GOC_Llamada_in_Alta_llamada';
				$View = 'GOC_Alta_llamada_DV_Master_View';
				break;
			case 'ModificacionCuentas':
				$dbName = 'GOC_Modificacion_Cuentas';
				$View = 'GOC_Modificacion_Cuentas_DV_Master_View';
				break;
			default:
				return "view not contemplated YET";
				break;
		}
	$columnNames = getColumnNamesItem($View, $dbName);
	$columnNamesAndTypes = getColumnNamesAndTypesItem($View, $dbName);
	$columnNamesString = getColumnNamesStringItem($columnNames);
	$columnSumNameString = getSumColumnNamesStringItem($columnNamesAndTypes);
    $table_name = $View;
	
	$key=getPKItem($View, $dbName);
    $params = $cols = $totalRecords = $data = $totalSum= array();

    $params = $_REQUEST;
    $cols = $columnNames;
		$colsFilteredNumber;
		$colsFilteredSearch;
	foreach ($params["columns"] as $i => $column) {
		if(!empty($column["search"]["value"])){
			$colsFilteredNumber[] = $i;
			$colsFilteredSearch[] = $column["search"]["value"];
		}
	}

	$where_condition = $sqlTot = $sqlRec = $sqlSumTotal = "";

	if(!empty($params['search']['value'])){
		$where_condition .= "AND ( ";
		foreach ($columnNames as $i => $column) {
			if($i == 0 ){
				$where_condition .= "$column LIKE '%".$params['search']['value']."%' ";
			}else{
				$where_condition.= "OR $column LIKE '%".$params['search']['value']."%' ";
			}
			if($i == (count($columnNames)-1)){
				$where_condition.= ")";
			}
		}
	}
	if(isset($colsFilteredNumber)){
		//for every column that has a filter on we get their number
		foreach ($colsFilteredNumber as $ikey => $index) {
			//Grab the value of that column search and separate the range filter and column filter with |
			$searchValueArray = explode("|", $colsFilteredSearch[$ikey]);
			foreach ($searchValueArray as $key2 => $filterSearch) {
				if($filterSearch !=""){
					//do the query depending if it is a range filter or a column search
					if(strpos($filterSearch, "Range;") !== false){
						$where_condition.= "AND ( ".$cols[$index];
						//separate the min and max range
						$rangeQueryArray= explode(";", $filterSearch);
						//query for range
						if($rangeQueryArray[3]=="DATE"){
							$where_condition.=" BETWEEN '".$rangeQueryArray[1]."' AND '".$rangeQueryArray[2]."' ) ";
						}else{
							$where_condition.=" BETWEEN ".$rangeQueryArray[1]." AND ".$rangeQueryArray[2]." ) ";
						}
					}else{
						//query for column filter
						$where_condition.= "AND ( ".$cols[$index]." LIKE '%".$filterSearch."%' ) ";
					}
				}
			}

		}

	}

	$sql_query = "SELECT $key,".($columnNamesString)." FROM $dbName.dbo.$View";
	$sql_query_sum_total = "SELECT null, ".($columnSumNameString)." FROM $dbName.dbo.$View ";
	// foreach ($columnNames as $faKey => $colName) {
	// 	$sql_query_sum_total.=", $colName";
	// }

	$sqlTot.=$sql_query;
	$sqlRec.=$sql_query;
	$sqlSumTotal.=$sql_query_sum_total;


	if(isset($where_condition) && $where_condition != ''){
		$sqlTot.= $where_condition;
		$sqlRec.= $where_condition;
		// uncomment if you want the totals to be filtered too
		// $sqlSumTotal.= $where_condition;
	}

	$orderBy = $params['order'][0]['column'];
	//when in views that have actions, an extra column is added at the beggining, shifting the whole ennumeration by 1
	if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0 && $orderBy!=00)
	{
		$orderBy--;
	}

	if($params['length'] != -1){
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir']." OFFSET "
		.$params['start']." ROWS FETCH NEXT ".$params['length']." ROWS ONLY";
	}else{
		$sqlRec.= " ORDER BY ".$cols[$orderBy]." ".$params['order'][0]['dir'];
	}

	// $queryTot = mysqli_query($conexion, $sqlTot)or die("Database error: ". mysqli_error($conexion)." \nsql: ".$sqlTot);
	$queryTot = sqlsrv_query($conexion, $sqlTot,array(), array( "Scrollable" => 'keyset' ));
	if($queryTot == false){
		echo "error here";
		die( print_r( sqlsrv_errors(), true));
	} 
	$totalRecords = sqlsrv_num_rows($queryTot);
	if ($totalRecords === false){
		die( print_r( sqlsrv_errors(), true));
		echo "Error in retrieveing row count.";
	}


	$queryRecords = sqlsrv_query($conexion, $sqlRec);
	if($queryRecords == false){
		echo($sqlRec);
		die( print_r( sqlsrv_errors(), true));
	} 
	$querySumTotals = sqlsrv_query($conexion, $sqlSumTotal);
	if($querySumTotals == false){
		echo($sqlSumTotal);
		die( print_r( sqlsrv_errors(), true));
	} 
	while($row = sqlsrv_fetch_array($querySumTotals,SQLSRV_FETCH_NUMERIC )){
		array_splice($row,0, 1);
		$totalSum[] = $row;
	}
	// echo json_encode($totalSum);

	while( $row = sqlsrv_fetch_array($queryRecords, SQLSRV_FETCH_NUMERIC)){
		// REMOVED WHEN DATETIME FIXED
		foreach($row as $faKey => $faValue){
			if($faValue instanceof DateTime){
				$row[$faKey] = $faValue->format("Y-m-d");
			}
		}
		// ALO REMOVED
		$row=utf8ize($row);

		// if(!in_array($View, array("FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView")) && strcmp($Rol,"admin")===0)
		// {
		// 	if($row[count($row)-1] == '0') {
		// 		$row[0]= "<td style=\"width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"><i class=\"material-icons\">edit</i></a>	
		// 		<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Act\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">replay</i></a></td>";
		// 	}
		// 	else {
		// 		$row[0]= "<td style=\"text-align:center;width:40px;\"> <a href=\"./Form.php?Id=$key" . "_" . $row[0] . "&ViewName=$View\"> <i class=\"material-icons\">edit</i> </a>	
		// 		<a href=\"./DeleteCode.php?Id=$table_name" . "_" . "$key" . "_" . $row[0] . "&ActOrInact=Inact\" onclick=\"return confirm('Está seguro de que quiere borrar este registro?')\"> <i class=\"material-icons\">delete</i></a></td>";
		// 	}
		// }else{
		// 	array_splice($row,0, 1);
		// }
		$data[] = $row;
		}
	$json_data = array(
		"draw"          =>intval($params['draw']),
		"recordsTotal"  =>intval($totalRecords),
		"recordsFiltered" => intval($totalRecords),
		"data"          =>$data,
		"totals"		=>$totalSum,
		"query"			=>$sqlRec,
		"table"			=>$View,
		"key"			=>$key,
		"sum"			=>$sqlSumTotal
	);
    return json_encode($json_data);

        
	}
	// to help sanitize jsons
	function utf8ize($d) {
		if (is_array($d)) {
			foreach ($d as $k => $v) {
				$d[$k] = utf8ize($v);
			}
		} else if (is_string ($d)) {
			return utf8_encode($d);
		}
		return $d;
	}
	function getPKItem($ViewName, $dbName) 
{
	include ('Mysqlconn.php');
	$table=$ViewName;
	// MODIFICAR: guion bajo (?)
	// $querydata="SHOW KEYS FROM ARista.dbo.$table where Key_name = 'PRIMARY'";
	$querydata = "SELECT CONSTRAINT_NAME FROM $dbName.INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = '$table' AND CONSTRAINT_TYPE ='PRIMARY KEY'";
	$data_sql = sqlsrv_query( $conexion, $querydata);
	if($data_sql == false){
		echo( print_r( sqlsrv_errors(), true));
		die("Error to Get the $ViewName details.");
	} 
	$data=array();

	while ($row = sqlsrv_fetch_array($data_sql, SQLSRV_FETCH_NUMERIC)) 
	{
		$data = $row;		
	}
	// WHAT IS THE PK of these tables?
	$PK=explode("_", $data[0])[2];

	return $PK;
}
	function getColumnNamesItem($ViewName, $dbName) {
    
		include ("Mysqlconn.php");
	
		$querycolumns_names="SELECT (COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG = '$dbName' AND TABLE_NAME = '$ViewName' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
		$columnNames_sql = sqlsrv_query( $conexion, $querycolumns_names);
	
		$columnNames = array();
	
		//if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");
	
		while ($row = sqlsrv_fetch_array($columnNames_sql, SQLSRV_FETCH_NUMERIC)) 
		{
			if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
			array_push($columnNames, $row[0]);	
		}
	
	
	
		return $columnNames;
	}

	function getColumnNamesAndTypesItem($tableName, $dbName){
		include ("Mysqlconn.php");
		
		$querycolumns_names="SELECT COLUMN_NAME FROM information_schema.COLUMNS WHERE TABLE_CATALOG = '$dbName' AND TABLE_SCHEMA = 'dbo' AND TABLE_NAME = '$tableName' AND COLUMN_NAME NOT LIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%';";
		$columnNames_sql =sqlsrv_query($conexion, $querycolumns_names);
	
		$columnNames = array();
	
		//if($ViewName=="OportunidadesView") array_push($columnNames, "IdOportunidad");
	
		while ($row = sqlsrv_fetch_array($columnNames_sql, SQLSRV_FETCH_NUMERIC)) 
		{
			if (strpos($row[0], 'Silly') !== false || strpos($row[0], 'Logs') !== false) continue;
			array_push($columnNames, $row[0]);	
		}
	
	
		$query="SELECT DATA_TYPE FROM information_schema.COLUMNS where TABLE_SCHEMA = 'dbo' AND TABLE_CATALOG='$dbName' AND TABLE_NAME = '$tableName' AND COLUMN_NAME NOT lIKE 'Checksum%' AND COLUMN_NAME NOT LIKE 'Id%' AND COLUMN_NAME NOT LIKE 'Avatar%'";
		$Views_sql = sqlsrv_query( $conexion, $query);
		if( !$Views_sql )
	{
		 echo ( print_r( sqlsrv_errors(), true));
	}
		$types=array();
	
		while ($row = sqlsrv_fetch_array($Views_sql, SQLSRV_FETCH_NUMERIC)) 
		{
			array_push($types,$row[0]);	
		}
		$columns = array();
	
		foreach ($columnNames as $i => $name) {
			array_push($columns, ["name" => $name, "type" => $types[$i], "width" => checkLongField($name)]);
		}
		return $columns;
	}
	function getColumnNamesStringItem($columnNames) 
{
	$columnsString="";

	for($i=0;$i<sizeof($columnNames);$i++) 
	{
		$columnsString = $columnsString . $columnNames[$i] . ", ";
	}

	// $columnsString = $columnsString . "IdEstadoActividad";
	$columnsString = $columnsString;

	//$columnsString = substr($columnsString, 0, -2);

	return $columnsString;
}
function getSumColumnNamesStringItem($columnNamesAndTypes) 
{
	$numericType = array("int", "long", "smallint", "bigint", "float", "numeric", "double", "decimal");
	$columnsString="";
	foreach ($columnNamesAndTypes as $faKey => $nameAndType) {
		if(array_search($nameAndType["type"], $numericType )!== false){
			$columnsString.="SUM(".$nameAndType['name']."), ";
		}else{
			$columnsString.="null, ";
		}
	}

	$columnsString = $columnsString . "IdEstadoActividad";

	return $columnsString;
}

?>