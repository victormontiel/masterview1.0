    function startItemTable(){
        $(".item-tab-menu").click(function(){
            console.log("click");
            let created = $(this).data("created");
            let itemAlias = $(this).attr("id").split("-")[1];

            if(!created){
                $("#Preloader").show()
                // Create table
                $.ajax({
            type: "POST",
            url: url_reqs,
            data: {"create_item_table" : true, "Rol": userRol, "item": itemAlias},
            dataType: "json",
            success: function (response) {
                
                let cells=[];
                let table = "";
                let col = 0;
                response.forEach(model => {
                    table += "<div class=\"table_container\" id=\"div_"+model.name+"\" style=\"display:none; position:absolute; top: 4%; left: 4%; width:100%; table-layout:fixed\">";
                    table += "<table id=\""+model.name+"\" class=\"display dataTable\" cellspacing=\"0\" width=\"100%\">";
                    table += "<thead>";
                    table += "<tr role=\"row\">";
                    let footer ="<tfoot>";
                    let frow1 = "<tr>";
                    let frow2 ="<tr id=\"partials_"+model.name+ "\">";
                    let frow3="<tr id=\"totals_" +model.name+ "\">";
                    let frow4="<tr id=\"minimums_" +model.name+ "\">";
                    let frow5="<tr id=\"maximums_" +model.name+ "\">";
                    if(!exceptionsView.includes(model.name) && userRol =="admin")
                    {
                        table += "<th class=\"actions\" tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"left\" aria-label=\"Actions\" display=\"inline-block;\" type=\"hidden\">Actions</th>";
                        frow1+="<td class='actionsColumn'></td>";
                        frow2+="<td class='actionsColumn'></td>";
                        frow3+="<td class='actionsColumn'></td>";
                        frow4+="<td class='actionsColumn'></td>";
                        frow5+="<td class='actionsColumn'></td>";
                    }                   

                    model.columns.forEach(column => {
                        cells[col++]={"filter": "", "range":"", "active":false, "max": "", "min": "", "type": column.type};
                        let sum ="";
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        })){
                            sum = "class=\"sum\"";
                            frow2+="<td class=\"partials\" style=\"display:none;\"></td>";
                            frow3+="<td class=\"totals\" style=\"display:none;\"></td>";	
                        }else{
                            frow2+="<td class=\"emptyPartials\"></td>";
                            frow3+="<td class=\"emptyTotals\"></td>";
                        }
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        }) || column.type.toLowerCase() == 'date'){
                            frow4+="<td class=\"minimum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                            frow5+="<td class=\"maximum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                        }else{
                            frow4+="<td class=\"emptyMinimum\" data-type='empty'></td>";
                            frow5+="<td class=\"emptyMaximum\" data-type='empty'></td>";
                        }
                        table+= "<th max-height: 50px; tabindex=\"0\" " +sum;
                        table+= " style=\"text-align:left;"+column.width+"\" rowspan=\"1\" colspan=\"1\" aria-label=\"";
                        table+= column.name+": activate to sort column descending\" display=\"inline-block\" aria-sort=\"ascending\" type=\"hidden\">"+column.name+"</th>";
                        frow1+="<td class=\"columnSearcher Filter"+model.name+"\" style=\"display:none;\">Buscar</td>";

                    });
                    table+="</tr> </thead>";
                    table+="<tbody></tbody>";
                    frow1+="</tr>";
                    frow2+="</tr>";
                    frow3+="</tr>";
                    frow4+="</tr>";
                    frow5+="</tr>";
                    filtersMap[model.name]=cells;
                    footer+=frow1+frow2+frow3+frow4+frow5+"</tfoot>";
                    table+=footer;
                    table+="</table>";
                    table+="</div>";

                });
                $("#table_container").append(table);
                
                dtable = document.getElementById(model.name);
                let currentTable = $("#"+dtable.id).DataTable({
                // "bProcessing":true,
                // "Processing": true,
                "serverSide":true,
                "ajax":{
                    url: url_reqs,
                    type: "POST",
                    data:{"fetch_item_data":true, "item": dtable.id, "Rol": userRol},
                    error: function(){
                        $("#dataTable_processing").css("display","non");
                    },
                
                },
                "columnDefs":[{ 
                "targets":'actions',
                "orderable":false
                }],
                scrollX:true,
                select: true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json",
                        select:{
                            rows:", %d filas seleccionadas"
                        }
                    },
                    dom: 'Blfrtip',
                    buttons : [
                        {
                            text: 'Filtros',
                            action: function(e, dt, node, config){
                                let rows = document.getElementsByClassName('columnSearcher');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide=0;
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                hide=1;   
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner(dtable.id);
                                            }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                            },
                            "className":'btn btn-filtros'
                        },
                        {
                            text: 'Rangos',
                            action: function (e, dt, node, config) {
                                        let mins = document.getElementsByClassName('minimum');
                                        let maxs = document.getElementsByClassName('maximum');
                                        for(let i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                            
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('partials');
                                        let raws = document.getElementsByClassName('emptyPartials');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('totals');
                                        let raws = document.getElementsByClassName('emptyTotals');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                                rows[i].setAttribute('style', '');
                                                raws[i].setAttribute('style', '');
                                                hide=0;
                                                
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                raws[i].style.display='none';
                                                hide=1;
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },
                                {
                                    extend: 'selectAll',
                                        action : function(e, dt, button, config){
                                            selectbool = true;
                                            getAll(dtable.id, selectAll);
                                            
                                        },
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    bBomInc: true,
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected',
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))',
                                },
                                "className":'btn'
                                },
                                {
                                  text: 'Nuevo Registro<i class=\"material-icons right\" >add_box</i>',
                                  action: function ( e, dt, button, config ) {
                                    window.location = 'Form.php?ViewName='+dtable.id;
                                  },
                                  "className":'btn pulse'        
                                }
                    ],
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],                    
                    initComplete: function () {
                        if(exceptionsView.includes(dtable.id) || userRol !="admin"){
                            $('#'+dtable.id).DataTable().button(".pulse").remove();
                        }
                    },
                    footerCallback: function (row, data, start, end, display) {
                        let api=this.api();
                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        let num = index;
                        let sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                            let cell = $('tr:eq(1) td:eq(' + num + ')', currentTable.table().footer());
                            cell.html(parseFloat(sum).toFixed(2));
                        });

                        let totalsJson =api.ajax.json().totals[0];
                        totalsJson.forEach((element, index) => {
                        let num = index;

                        let sum = element;
                        if(sum === null){
                            sum =0;
                        }

                        if(sum >= 0){
                            let cell = $('tr:eq(2) td:not(".actionsColumn"):eq(' + num + ')', currentTable.table().footer());
                            if($(cell).hasClass("totals")){
                            cell.html(parseFloat(sum).toFixed(2));
                            }
                        }
                        });
                    },
                    preDrawCallback: function() {
                        $("#Preloader").show();
                    },
                    drawCallback: function(){
                        $("#Preloader").hide();
                    }

                });

                $('#' + dtable.id + ' tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                } );


            selectInitiate();
            searchersInitiate();
            activateFilterSearch();   

                $("#Preloader").hide()
                
                displaytable(itemAlias+'View');
            }
        });
            }else{
                displaytable(itemAlias+'View');
            }
        });
    }