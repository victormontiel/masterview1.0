<?php
require_once("functions_EM_2.php");
require_once("functions_2.php");

if(isset($_GET["left_menu"])){
    echo createLeftBar(retrieveViewList(), $_GET["Rol"]);
}
if(isset($_GET["logout"])){
    session_destroy();
    header('Location: ./Login.php'); 
}
if(isset($_POST["create_tables"])){
    echo createAndHideTables($_POST["Rol"]);
}
if(isset($_POST["fetch_data"])){
    echo getDataTable($_POST["view"], $_POST["Rol"]);
}
if(isset($_GET["create_tabs_forms"])){
    echo createTabsAndForms($_GET["ViewName"], $_GET["action"], $_GET["id"], $_GET["Rol"]);
}

if(isset($_GET["innerMenu"])){
    echo createInnerMenu($_GET["Rol"], $_GET["db_load"]);
}
if(isset($_GET["create_item_table"])){
    echo createItemTable($_GET["Rol"], $_GET["item"]);
}
if(isset($_GET["fetch_item_data"])){
    echo fetchItemData($_GET["item"], $_GET["Rol"]);
}

?>