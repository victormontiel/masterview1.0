    function openTab(tab_name){ 
        var alltabs = document.getElementsByClassName("tab-menu");
        var tab = document.getElementById(tab_name);

   for(i = 0; i <  alltabs.length; i++) {
            alltabs[i].style.backgroundColor = "";
            alltabs[i].style.color = "";
        }

        tab.style.color = "#00ffd1";
        tab.style.backgroundColor = "#15202b";
        tab.style.borderRadius = "3px";
        tab.style.width = "auto";

    }

    function setInnerText(obj, text) {
                var object = document.getElementById(obj);
                object.innerText = text;
            }
    
    function displaytable(table) 
            {
                $(".table_container").hide();
                var y = document.getElementById("div_" + table.trim());
                y.style.display = 'block';
                $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
            }

    let url_reqs = "functions_route.php";

    function buildLeftBar(){
        $.ajax({
            type: "GET",
            url: url_reqs,
            data: {"left_menu" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                let menu = "<ul>";
                response.tabs.forEach(element => {
                    menu += "<li><a class=\"tab-menu\" id=\"tab-"+element+"\" onclick=\"openTab('tab-"+element+"'),setInnerText('BHeader', '"+element+" - Vista datos')\"href=\"javascript:displaytable('"+element+"View');\">"+element+"</a></li>"
                });
                menu +="</ul>";
                $("#MantenimientoArista").html(menu);
            }
        });
    }
    function selectInitiate(){
            $("select").val('10');
            $('select').addClass("browser-default");
            $('select').material_select();
            }

    function searchersInitiate(){

            $('.ColumnSearcher').each( function () {
            let title = "Buscar";
            $(this).html( '<input type="text" placeholder="'+title+'" />' );
            });

            $('.minimum').each( function () {
            let title = "Min";
            $(this).html( '<input type="text" placeholder="'+title+'" value =""/>' );
            });

            $('.maximum').each( function () {
            let title = "Max";
            $(this).html( '<input type="text" placeholder="'+title+'" value =""/>' );
            });
        }
    let filtersMap = {};
    let exceptionsView = ["FacturasView", "GruposView", "RolesView", "UsuariosView", "MembresiasView"];
    let exceptionsFormat = ["int","long", "double", "decimal"];
    function buildTableContainer(dataTableFunc){
        $.ajax({
            type: "POST",
            url: url_reqs,
            data: {"create_tables" : true, "Rol": userRol},
            dataType: "json",
            success: function (response) {
                
                let tables = "";
                
                response.tables.forEach(model => {
                    let cells=[];
                    let table = "";
                    let col = 0;
                    table += "<div class=\"table_container\" id=\"div_"+model.name+"\" style=\"display:none; position:absolute; top: 4%; left: 4%; width:100%; table-layout:fixed\">";
                    table += "<table id=\""+model.name+"\" class=\"display dataTable\" cellspacing=\"0\" width=\"100%\">";
                    table += "<thead>";
                    table += "<tr role=\"row\">";
                    let footer ="<tfoot>";
                    let frow1 = "<tr>";
                    let frow2 ="<tr id=\"partials_"+model.name+ "\">";
                    let frow3="<tr id=\"totals_" +model.name+ "\">";
                    let frow4="<tr id=\"minimums_" +model.name+ "\">";
                    let frow5="<tr id=\"maximums_" +model.name+ "\">";
                    if(!exceptionsView.includes(model.name) && userRol =="admin")
                    {
                        table += "<th class=\"actions\" tabindex=\"0\" rowspan=\"1\" colspan=\"1\" align=\"left\" aria-label=\"Actions\" display=\"inline-block;\" type=\"hidden\">Actions</th>";
                        frow1+="<td class='actionsColumn'></td>";
                        frow2+="<td class='actionsColumn'></td>";
                        frow3+="<td class='actionsColumn'></td>";
                        frow4+="<td class='actionsColumn'></td>";
                        frow5+="<td class='actionsColumn'></td>";
                    }                   

                    model.columns.forEach(column => {
                        cells[col++]={"filter": "", "range":"", "active":false, "max": "", "min": "", "type": column.type};
                        let sum ="";
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        })){
                            sum = "class=\"sum\"";
                            frow2+="<td class=\"partials\" style=\"display:none;\"></td>";
                            frow3+="<td class=\"totals\" style=\"display:none;\"></td>";	
                        }else{
                            frow2+="<td class=\"emptyPartials\"></td>";
                            frow3+="<td class=\"emptyTotals\"></td>";
                        }
                        if(exceptionsFormat.some(function(element){
                            return column.type.toLowerCase().includes(element);
                        }) || column.type.toLowerCase() == 'date'){
                            frow4+="<td class=\"minimum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                            frow5+="<td class=\"maximum\" data-type="+column.type+" data-value=\"\" style=\"display:none;\"></td>";
                        }else{
                            frow4+="<td class=\"emptyMinimum\" data-type='empty'></td>";
                            frow5+="<td class=\"emptyMaximum\" data-type='empty'></td>";
                        }
                        table+= "<th max-height: 50px; tabindex=\"0\" " +sum;
                        table+= " style=\"text-align:left;"+column.width+"\" rowspan=\"1\" colspan=\"1\" aria-label=\"";
                        table+= column.name+": activate to sort column descending\" display=\"inline-block\" aria-sort=\"ascending\" type=\"hidden\">"+column.name+"</th>";
                        frow1+="<td class=\"columnSearcher Filter"+model.name+"\" style=\"display:none;\">Buscar</td>";

                    });
                    table+="</tr> </thead>";
                    table+="<tbody></tbody>";
                    frow1+="</tr>";
                    frow2+="</tr>";
                    frow3+="</tr>";
                    frow4+="</tr>";
                    frow5+="</tr>";
                    filtersMap[model.name]=cells;
                    footer+=frow1+frow2+frow3+frow4+frow5+"</tfoot>";
                    table+=footer;
                    table+="</table>";
                    table+="</div>";

                    tables += table;

                });
                $("#Preloader").hide()
                $("#table_container").html(tables);

                dataTableFunc();
                
            }
        });
    }
    function columnSearcherCleaner(View) {
            $('.Filter' + View).each( function () {
                if($(this).find('input').val().length) 
                {
                    $(this).find('input').attr("placeholder", "Buscar");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
                                                                          
            });
        }
        function minCleaner() {
            $('.minimum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Min");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }

        function maxCleaner() {
            $('.maximum').each( function () {
                if($(this).find('input').val()) {
                    $(this).find('input').attr("placeholder", "Max");
                    $(this).find('input').val('');
                    $(this).find('input').change();
                }
            });
        }
    
    function buildTableContent(){
        let dts = Array.from(document.getElementsByClassName('dataTable'));
            dts.forEach(dtable => {
                let currentTable = $("#"+dtable.id).DataTable({
                // "bProcessing":true,
                // "Processing": true,
                "serverSide":true,
                "ajax":{
                    url: url_reqs,
                    type: "POST",
                    data:{"fetch_data":true, "view": dtable.id, "Rol": userRol},
                    error: function(){
                        $("#dataTable_processing").css("display","non");
                    },
                
                },
                "columnDefs":[{ 
                "targets":'actions',
                "orderable":false
                }],
                scrollX:true,
                select: true,
                    autoWidth : true,
                    "language": {
                        "sUrl": "Language/Spanish.json",
                        select:{
                            rows:", %d filas seleccionadas"
                        }
                    },
                    dom: 'Blfrtip',
                    buttons : [
                        {
                            text: 'Filtros',
                            action: function(e, dt, node, config){
                                let rows = document.getElementsByClassName('columnSearcher');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            hide=0;
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                hide=1;   
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                columnSearcherCleaner(dtable.id);
                                            }
                                        else 
                                        {
                                                $(".btn-filtros").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                            },
                            "className":'btn btn-filtros'
                        },
                        {
                            text: 'Rangos',
                            action: function (e, dt, node, config) {
                                        let mins = document.getElementsByClassName('minimum');
                                        let maxs = document.getElementsByClassName('maximum');
                                        for(let i=0;i<mins.length;i++) {
                                            if(mins[i].style.display =='none') {
                                            mins[i].setAttribute('style', '');
                                            maxs[i].setAttribute('style', '');
                                            hide = 0;
                                            
                                        }
                                        else {
                                            mins[i].style.display='none';
                                            maxs[i].style.display='none';
                                            hide = 1;
                                        }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                                minCleaner();
                                                maxCleaner();
                                        }
                                        else 
                                        {
                                                $(".btn-rangos").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-rangos'
                                },
                                {
                                    text: 'Parciales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('partials');
                                        let raws = document.getElementsByClassName('emptyPartials');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                            rows[i].setAttribute('style', '');
                                            raws[i].setAttribute('style', '');
                                            hide=0;
                                            
                                        }
                                        else {
                                            rows[i].style.display='none';
                                            raws[i].style.display='none';
                                            hide=1;
                                        }
                                        }
                                       /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-parciales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-parciales'
                                },
                                {
                                    text: 'Totales',
                                    action: function (e, dt, node, config) {
                                        let rows = document.getElementsByClassName('totals');
                                        let raws = document.getElementsByClassName('emptyTotals');
                                        let hide;
                                        for(let i=0;i<rows.length;i++) {
                                            if(rows[i].style.display =='none') {
                                                rows[i].setAttribute('style', '');
                                                raws[i].setAttribute('style', '');
                                                hide=0;
                                                
                                            }
                                            else {
                                                rows[i].style.display='none';
                                                raws[i].style.display='none';
                                                hide=1;
                                            }
                                        }
                                        /***TOGGLE BUTTONS****/
                                        if(hide==1){
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#00f1c5");
                                                });
                                        }
                                        else 
                                        {
                                                $(".btn-totales").each(function() {
                                                    $(this).css("background-color", "#1d7d74");
                                                });
        
                                        }
                                        $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw;
                                    },
                                    "className":'btn btn-totales'
                                },
                                {
                                    extend: 'selectAll',
                                        action : function(e, dt, button, config){
                                            selectbool = true;
                                            getAll(dtable.id, selectAll);
                                            
                                        },
                                    text:'Todos<i class="material-icons right">done_all</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'selectNone',
                                    text:'Ninguno<i class="material-icons right">remove_circle_outline</i>',
                                    "className":'btn'
                                },
                                {
                                    extend: 'csv',
                                    bBomInc: true,
                                    text:'CSV<i class="material-icons right">file_download</i>',
                                    exportOptions: {
                                    columns: ':visible:not(:eq(0))',
                                    rows:'.selected',
                                },
                                "className":'btn'
                                },
                                {
                                extend: 'excel',
                                text:'EXCEL<i class="material-icons right">file_download</i>',
                                exportOptions: {
                                rows:'.selected',
                                columns: ':visible:not(:eq(0))',
                                },
                                "className":'btn'
                                },
                                {
                                  text: 'Nuevo Registro<i class=\"material-icons right\" >add_box</i>',
                                  action: function ( e, dt, button, config ) {
                                    window.location = 'Form.php?ViewName='+dtable.id;
                                  },
                                  "className":'btn pulse'        
                                }
                    ],
                    lengthMenu: [
                    [ 25, 50, -1 ],
                    [ '25', '50', 'Todos' ]
                    ],                    
                    initComplete: function () {
                        if(exceptionsView.includes(dtable.id) || userRol !="admin"){
                            $('#'+dtable.id).DataTable().button(".pulse").remove();
                        }
                    },
                    footerCallback: function (row, data, start, end, display) {
                        let api=this.api();
                        api.columns('.sum', {page: 'current'}).every(function (index) {
                        let num = index;
                        let sum = this
                        .data()
                        .reduce( function (a,b) {
                        return (parseFloat(a) || 0) + (parseFloat(b) || 0);
                        }, 0);
                            let cell = $('tr:eq(1) td:eq(' + num + ')', currentTable.table().footer());
                            cell.html(parseFloat(sum).toFixed(2));
                        });

                        let totalsJson =api.ajax.json().totals[0];
                        totalsJson.forEach((element, index) => {
                        let num = index;

                        let sum = element;
                        if(sum === null){
                            sum =0;
                        }

                        if(sum >= 0){
                            let cell = $('tr:eq(2) td:not(".actionsColumn"):eq(' + num + ')', currentTable.table().footer());
                            if($(cell).hasClass("totals")){
                            cell.html(parseFloat(sum).toFixed(2));
                            }
                        }
                        });
                    },
                    preDrawCallback: function() {
                        $("#Preloader").show();
                    },
                    drawCallback: function(){
                        $("#Preloader").hide();
                    }

                });

                $('#' + dtable.id + ' tbody').on( 'click', 'tr', function () {
                $(this).toggleClass('selected');
                } );

            });

            selectInitiate();
            searchersInitiate();
            activateFilterSearch();   
    }
    function getAll(dtableid, callback){
        if($("#"+dtableid).DataTable().page.len()!=-1){
            $("#"+dtableid).DataTable().page.len(-1).draw();
            $("#"+dtableid).on("draw.dt", function(){
                callback(dtableid);
            })
        }else{
            callback(dtableid);
        }

    }
    let selectbool = false;
    function selectAll(dtableid){
        if(selectbool){
        $("#"+dtableid).DataTable().rows({ search: 'applied'}).select();
        }
        selectbool=false;
    }
    function activateFilterSearch(){
        $(".dataTable").each(function (index, table) {
                $(table).find(".columnSearcher").each(function(i, cs){
                        $(cs).children('input').on('change', function(){
                            filterCell = filtersMap[$(table).attr("id")][i];
                            //assign the input value on the column
                            filterCell.filter = this.value;
                            //activate the cell if at least one field is not empty
                            filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                            //do search if cell is active
                            if(filterCell.active){
                                $(table).DataTable().columns(i)
                                        .search(filterCell.filter+"|"+filterCell.range);  
                            }else{
                                $(table).DataTable().columns(i)
                                        .search("");
                            }
                            $(table).DataTable().draw();
                        });
                    });
            $(table).find("#minimums_"+$(table).attr("id"))
            .find("td:not('.actionsColumn')")
            .each(function(i, td){
                $(td).children("input").on('change', function(){
                    let filterCell = filtersMap[$(table).attr("id")][i];
                    let max = filterCell.max;
                    filterCell.min=$(this).val();
                    let min = filterCell.min;
                    let search ="";
                    let empty = (max == "" && min=="");
                    
                    if(min!=""){
                        if(filterCell.type=="date"){
                            if(max==""){
                                max="3000-12-12";
                            }
                        }else{
                            if(max==""){
                                max=Number.MAX_SAFE_INTEGER;
                            }
                        }
                        search+="Range;"+min+";"+max+"";
                    }else{
                        if(max!=""){
                            if(filterCell.type =="date"){
                                min="1800-01-01";
                            }else{
                                min=-1*Number.MAX_SAFE_INTEGER;
                            }
                            search+="Range;"+min+";"+max+"";
                        }
                    }
                    filterCell.range=search;
                    //activate the cell if at least one field is not empty
                    filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                    //do search if cell is active
                    if(filterCell.active){
                        $(table).DataTable().columns(i)
                                .search(filterCell.filter+"|"+filterCell.range);  
                    }else{
                        $(table).DataTable().columns(i)
                            .search("");
                    }
                        $(table).DataTable().draw();
                });
            });
        $(table).find("#maximums_"+$(table).attr("id"))
            .find("td:not('.actionsColumn')")
            .each(function(i, td){
                $(td).children("input").on('change', function(){

                    let filterCell = filtersMap[$(table).attr("id")][i];
                    let min = filterCell.min;
                    filterCell.max=$(this).val();
                    let max = filterCell.max;
                    let search ="";
                    let empty = (min == "" && max=="");
                    
                    if(max!=""){
                        if(filterCell.type=="date"){
                            if(min==""){
                                min="1800-01-0";
                            }
                        }else{
                            if(min==""){
                                min=-1*Number.MAX_SAFE_INTEGER;
                            }
                        }
                        search+="Range;"+min+";"+max+"";
                    }else{
                        if(min!=""){
                            if(filterCell.type =="date"){
                                max="3000-12-12";
                            }else{
                                max=Number.MAX_SAFE_INTEGER;
                            }
                            search+="Range;"+min+";"+max+"";
                        }
                    }
                    filterCell.range=search;
                    //activate the cell if at least one field is not empty
                    filterCell.active= (filterCell.filter != "" || filterCell.range !="");
                    //do search if cell is active
                    if(filterCell.active){
                        $(table).DataTable().columns(i)
                                .search(filterCell.filter+"|"+filterCell.range);  
                    }else{
                        $(table).DataTable().columns(i)
                            .search("");
                    }
                        $(table).DataTable().draw();
                    
                });
            });
        });
    }
    
    $(document).ready(function(){
        
        buildLeftBar();
        $('.collapsible').collapsible();
        buildTableContainer(buildTableContent);
        
        $('.datepicker').pickadate(
                { 
                selectMonths: true,
                 selectYears: 15, 
                 today: 'Today',
                 format: 'yyyy-mm-dd', 
                 clear: 'Clear',
                 close: 'Ok',
                 closeOnSelect: false,
                 formatSubmit: 'yyyy-mm-dd'
                });
    
    });

