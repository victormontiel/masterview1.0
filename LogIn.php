<?php 
//If the password is wrong or not written (by entering another page before logging in) then this value ist sent via POST.

if(isset($_GET["wrong"]))
    {
        $wrong = $_GET["wrong"];
        if($wrong=='1')   echo "<script type='text/javascript'>alert('Las credenciales introducidas son incorrectas.');</script>";
        if($wrong=='2')   echo "<script type='text/javascript'>alert('Se ha cerrado la cuenta por inactividad. Vuelva a identificarse.');</script>";
    }

    ?>

<html>
	<head>
		<title>BilliB ARista</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- GOOGLE FONTS + ICONS -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="style/LogIn.css"/>
    <link rel="shortcut icon" type="image/png" href="https://billibfinance.com/wp-content/uploads/2017/10/favicom.png"/>
  </head>
  <body>
    <header id="BillibHeader"><h2 id="BHeader" align=\"center\">ARIsta_web. Aplicación de consulta y mantenimiento</h2>
    <!--Logo hyperlinked, apptitle, main-menu_dropdown--> 
    </header>
    <div id="logIn" class="container">
      <div class="row">
        <form class="col s6 offset-s3" method="POST" action="./EFirstPage.php">
          <div class="row">
            <div class="input-field col s12 center">
              <img src="images/BillibLogoTransparentLogIn.png" alt="Billib Logo" class="responsive-img">
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12 center">
                <span>Inicio de sesión</span>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 offset-s3">
              <i class="material-icons prefix">email</i>
              <input id="email" name="email" type="email" class="validate">
              <label for="email">Email</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s6 offset-s3">
              <i class="material-icons prefix">lock</i>
              <input id="password" name="password" type="password" class="validate">
              <label for="password">Contraseña</label>
          </div>
          <div class="row">
            <button class="btn waves-effect waves-light col s3 offset-s2" type="submit" name="logInUser">Entrar<i class="material-icons left">person</i></button>
            <button type="reset" class="btn waves-effect waves-light col s3 offset-s2" type="reset"><i class="material-icons left">refresh</i>Limpiar</button>
          </div>
        </form>
      </div>
    </div>
    <div class="footer">
      <footer id="BillibFooter">
        <p style="position: relative; left: -15px;  bottom: 13px; font-weight: 600; font-size: 13px; color: #3a3a3a;">Powered by dyTAB</p>
        <center><a href="http://proceedit.blogspot.com.es/" style="color:black; font-size: 13px; font-weight: 600;">Copyright © 2018 Proceedit, all rights reserved.</a>
      </footer>
    </div>
  </body>
</html>
